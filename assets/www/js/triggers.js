TBLCODE = {
    POL:{name:'POOL',
	  col:['owner','startlocation', 'startpos',
	       'endlocation', 'endpos', 'journey_start_time',
	       'journey_end_time','preferred_gender',
	       'totalmembers', 'occupied', 'freeseats',
	       'poolvehic', 'date_time']},
	  
    PRFL:{name:'PROFILE',
	  col:['user', 'type', 'type_change_date', 'endlocation', 'startlocation',
	       'start_work_time','end_work_time',
	       'gender', 'preferred_gender', 'avatar', 'pool', 'date_time', 'phone']},

    BZZ:{name:'BUZZ',
	  col:['buzzed_by', 'pool', 'buzz_msg',
	       'buzz_importance', 'date_time']},

    ATH:{name:'AUTH',
	  col:['user', 'pool', 'sky', 'did', 'date_time']},
    
    PCAL:{name:'POOLCALENDAR',
	  col:['pool', 'month','daysoff', 'dayson' , 'date_time']},
    
    PMEM:{name:'POOLMEMBERS',
	 col:['pool', 'user', 'startlocation', 'startpos',
	      'endlocation', 'endpos', 'distancefromowner', 'date_time']},

    PMCAL:{name:'POOLMEMBERS',
	 col:['pool', 'user', 'startlocation', 'startpos',
	      'endlocation', 'endpos', 'distancefromowner', 'date_time']}
};

TRIG_MAP = {
    DEL: trigCleanDB,
    INS: trigInsert,
    UPD: trigInsert
};

function processTriggers(trg){
    var status = false;
    for (var i=0; i < trg.length; i++){
        console.log('Code : ' + trg[i].CODE + ' : ' +  trg[i].DATA);
        if (TRIG_MAP.hasOwnProperty(trg[i].TYPE)){
    	    //console.log(trg[i].CODE + ' : ' +  trg[i].DATA);
            TRIG_MAP[trg[i].TYPE](trg[i].CODE,trg[i].DATA);
            console.log("After Insert call > " + i);
        }else{
            alert('unknow tigger code ' + trg[i].TYPE);
            return false;
        }
    }
}

function trigInsert(code,data){
    var table = TBLCODE[code].name;
    var columns= TBLCODE[code].col;
    console.log('Insert Tbl : ' + table);
    trigCleanDB(table);
    var sql='INSERT INTO '+table+' (';
    var values = "'";
    for(i=0;i<data.length;i++){
        //console.log(JSON.stringify(data[i]));
        for (j=0;j<columns.length;j++){
            var column = columns[j];
            if (data[i].hasOwnProperty(column)){
                sql += column + ',';
                values=values + data[i][column] + "','"
            }
            else{
                alert('unknow column ' +column + ' for table' + table);
            }
        }
    }

    sql=sql.substring(0,sql.length -1);
    values=values.substring(0,values.length -2);
    sql = sql + ') VALUES (' + values +')';
    console.log('FROM TRIG');
    console.log(sql);
    db.transaction(function (tx){
		tx.executeSql(sql);
    });
    console.log("After Insert");
}

function trigDropDB(code, data){
    db.transaction(function (tx){
	if (code==='ALL'){
	    tx.executeSql("DROP TABLE POOL");
	    tx.executeSql("DROP TABLE PROFILE");
	    tx.executeSql("DROP TABLE BUZZ");
	    tx.executeSql("DROP TABLE AUTH");
	    tx.executeSql("DROP TABLE POOLCALENDAR");
	    tx.executeSql("DROP TABLE POOLMEMBERS");
	}else if (code === 'SYNC'){
	    tx.executeSql("DROP TABLE POOL");
	    tx.executeSql("DROP TABLE PROFILE");
	    tx.executeSql("DROP TABLE BUZZ");
	    tx.executeSql("DROP TABLE POOLCALENDAR");
	    tx.executeSql("DROP TABLE POOLMEMBERS");
    }else{
	    tx.executeSql("DROP TABLE "+code);
	}
    });
}

function trigCleanDB(code, data){
    db.transaction(function (tx){
	if (code==='ALL'){
	    tx.executeSql("DELETE FROM POOL");
	    tx.executeSql("DELETE FROM PROFILE");
	    tx.executeSql("DELETE FROM BUZZ");
	    tx.executeSql("DELETE FROM AUTH");
	    tx.executeSql("DELETE FROM POOLCALENDAR");
	    tx.executeSql("DELETE FROM POOLMEMBERS");
	}else if (code === 'SYNC'){
	    tx.executeSql("DELETE FROM POOL");
	    tx.executeSql("DELETE FROM PROFILE");
	    tx.executeSql("DELETE FROM BUZZ");
	    tx.executeSql("DELETE FROM POOLCALENDAR");
	    tx.executeSql("DELETE FROM POOLMEMBERS");
    }else{
	    tx.executeSql("DELETE FROM "+code);
	}
    });
}


