//var domain = 'http://192.168.43.124:8000/mapp/';
//var domain = 'http://10.0.2.2:8000/mapp/';
var domain = 'http://carpoolindia.herokuapp.com/mapp/';
var opts = {
    type:'GET',    
    data:'',
    url:''
};
var did='';
var sky='';

var profile = {
    loaded:false
};

var profileLen = 0;

var pool = [];
var buzz = [];
window.auth = {};
window.poolLen = 0;
window.poolmembersLen = 0;
window.buzzLen = 0;
window.poolmembers = [];
window.poolcalendar = {};
var url = '';
var user;
var main = $("#main");
var dashboard = $("#dashboard");
var register = $("#register");
var login = $("#login");
var profupd = $("#profupd");
var carupd = $("#carupd");
var forgot = $("#forgot");
var homelink = $('.homelink');   // is not working while setting href
var homelink1 = $('#homelink1'); // is not working while setting href

var run =  false;
var devready = false;
var uiready = false;
var dashstartpoint = '';

/*
    Dashboard Load Points
    A - Only auth is present. So We still load dash
    B - After successful login, change pass
    C - After successful registration for Car Pool Member (Rider)
    D - After Successful registration for Car Owner (completes update of pool details)
    E - Onload & Profile is update
*/
/* This code is used for Intel native apps */
var onDeviceReady = function () {
    $.ui.clearHistory();
    devready = true;
    if (!run && uiready){
        console.log("device ready");
	$('#homelink_logreg').attr('href','#logregr'); // login register and forgot home will to first page
	$('#homelink_profup').attr('href','#dashboard');
	$('#dashboard').swipeUp(swipeUp);
        window.db = window.openDatabase("carpool", "1.0", "Car Pool", 200000);
        db.transaction(populateDB, errorCB, successCB);
        did = device.uuid;
        db.transaction(authcheck);
    }
};

// function swipeRight(){
//     $.ui.toggleSideMenu(true);
// }
function swipeUp(){
    hideNotif();
}

function authcheck (tx){
    tx.executeSql('SELECT * FROM AUTH', [],
        function(tx,results){
            var len = results.rows.length;
            if (len >= 1){
                auth = results.rows.item(0);
                user = auth.user;
                sky = auth.sky;
                dashstartpoint = 'A'; // Auth present
                // TODO: Sync seems too complicated
                // Has to be well written
                //ajaxSyncData();
                console.log('Auth found');
                //db.transaction(loadData);
                loadDashboard('onload', '', dashstartpoint);
            } else if (lsget('regdone') == 'True'){
                console.log('Reg Done');
                setNotif('Activation link has been sent to your email address. Please check your email and activate');
                homelink.attr('href', '#login');
                $.ui.loadContent("login",false,false,"slide");
            } else{
                console.log('Auth Not found 1');
                $.ui.loadContent("logregr",false,false,"slide");
                homelink.attr('href', 'logregr');
            }
        },
        function (err){
            console.log('Profile Not found 2');
            // Change Home link
            $.ui.loadContent("logregr",false,false,"slide");
            homelink.attr('href', 'logregr');
    	    // $('#homelink1').attr("href", "#logregr");
            alert("Error processing SQL:"+err.code);
        }
    );
}

function lsSet(name, data){
    console.log('LsSET : ' + name + ' : ' + data);
    if (!isNumber(data)){
        localStorage.setItem(name,JSON.stringify(data));
    } else{
        localStorage.setItem(name,data);
    }
}

function lsget(name){
    var data = localStorage.getItem(name);
    console.log('LsGET : ' + name + ' : ' + data);
    if (!isNumber(data)){
        return JSON.parse(data);
    } else{
        return data;
    }
}

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function loadData(tx){
    tx.executeSql('SELECT * FROM PROFILE', [],
        function(tx,results){
            lsSet('profileLen', results.rows.length);
            console.log("Profile Len 1" + lsget('profileLen'));
            if (results.rows.length >= 1){
                lsSet('profile', results.rows.item(0));
            } else{
                lsSet('profile', '');
                console.log('Profile Not found');
            }

            if (results.rows.length){
                dashstartpoint = 'E'; // Complete
                tx.executeSql('SELECT * FROM POOL', [],
                    function(tx,results){
                        lsSet('poolLen', results.rows.length);
                        if (results.rows.length > 0){
                            lsSet('pool', results.rows.item(0));
                        }
                        if (results.rows.length >= 1){
                            tx.executeSql('SELECT * FROM POOLMEMBERS', [],
                                function(tx,results){
                                    window.poolmembersLen = results.rows.length;
                                    for (i=0; i<poolmembersLen; i++){
                                        window.poolmembers[i] = results.rows.item(i);
                                    }
                                }, errorCB
                            );
                            tx.executeSql('SELECT * FROM POOLCALENDAR', [],
                                function(tx,results){
                                    if (results.rows.length > 0){
                                        window.poolcalendar = results.rows.item(0);
                                    }
                                } , errorCB
                            );
                        }
                        tx.executeSql('SELECT * FROM BUZZ', [],
                            function(tx,results){
                                    window.buzzLen = results.rows.length;
                                    for (i=0; i<buzzLen; i++){
                                        window.buzz[i] = results.rows.item(i);
                                    }
                            }, errorCB
                        );
                    }, errorCB
                );
            } else{
                homelink.attr('href', '#profupd');
                $.ui.loadContent("profupd",true,false,"slide");
            }
        }, errorCB
    );

}
function populateDB(tx) {
     //tx.executeSql('CREATE TABLE IF NOT EXISTS DATA (type TEXT, char1 TEXT, char2 TEXT, char3 TEXT, int1 INTEGER, int2 INTEGER, int3 INTEGER, datetime DATETIME, datetime1 DATETIME, datetime2 DATETIME)');
    tx.executeSql('CREATE TABLE IF NOT EXISTS POOL (id INTEGER PRIMARY KEY, owner TEXT, startlocation TEXT, startpos TEXT, endlocation TEXT, endpos TEXT, journey_start_time DATETIME, journey_end_time DATETIME, preferred_gender TEXT, totalmembers INTEGER, occupied INTEGER, freeseats INTEGER, poolvehic TEXT, date_time DATETIME)');
    tx.executeSql('CREATE TABLE IF NOT EXISTS PROFILE (id INTEGER PRIMARY KEY, user TEXT, type TEXT, type_change_date DATETIME, start_work_time DATETIME, end_work_time DATETIME, gender TEXT, pool TEXT, date_time DATETIME, phone INTEGER, startlocation text, endlocation text, startpos text, endpos text, preferred_gender TEXT, avatar TEXT)');
    tx.executeSql('CREATE TABLE IF NOT EXISTS BUZZ (id INTEGER PRIMARY KEY, buzzed_by TEXT, pool TEXT, buzz_msg TEXT, buzz_importance TEXT, date_time DATETIME)');
    tx.executeSql('CREATE TABLE IF NOT EXISTS AUTH (id INTEGER PRIMARY KEY, user TEXT, pool TEXT, sky TEXT, did TEXT, date_time DATETIME)');
    tx.executeSql('CREATE TABLE IF NOT EXISTS POOLCALENDAR (id INTEGER PRIMARY KEY, pool TEXT, month INTEGER, daysoff TEXT, dayson TEXT , date_time DATETIME)');
    tx.executeSql('CREATE TABLE IF NOT EXISTS POOLMEMBERS (id INTEGER PRIMARY KEY, pool TEXT, user TEXT, startlocation TEXT, startpos TEXT, endlocation TEXT, endpos TEXT, distancefromowner DECIMAL(3, 2), date_time DATETIME)');
}

function errorCB(err) {
    var str = "";
    for(var k in err){
        if (err.hasOwnProperty(k)){ //omit this test if you want to see built-in properties
                str += k + " = " + err[k] + "\n";
        }
    }
  alert("Error processing SQL:" + str);
  return '';
}

function successCB() {
    console.log("success!");
}

function queryDB(tx){
	tx.executeSql('SELECT * FROM DATA', [], querySuccess, errorCB);
}

function getInfo(tx){
	tx.executeSql("SELECT * FROM DATA WHERE type = 'ldetails'", [], querySuccess, errorCB);
}

function querySuccess(tx, results){
	return results;
}

function insertDB(tx){
	//tx.executeSql("INSERT INTO DATA (type, char1, char2, char3, int1, int2, int3, datetime, datetime1, datetime2) VALUES ('TEST','','','',0,0,0,'2014-01-01 00:00:00.00', '2014-01-01 00:00:00.00', '2014-01-01 00:00:00.00')");
}

function truncateDB(tx){
	tx.executeSql("DELETE FROM DATA");
}

function getData(){
    var opts = {
        type: 'GET',
        success: function (result){
            console.log(result);
        },
        url:'http://www.dealfreaks.in/asja/find?dealchar=dell'
    };
    $.ajax(opts);
}

$(document).ready(function(){
    $(".exit").click(function(){
        navigator.app.exitApp();
    });

    $("#logout").click(function(){
	$.ui.showMask('Exiting the application');
	db.transaction(function (tx){
            var sql = "DELETE FROM AUTH";
            tx.executeSql(sql);
            });
        navigator.app.exitApp();
    });
}
);

function setNotif(msg){
    notification = $('#notification');
    notification.hide();
    notifmsg = $('#notif-msg');
    notifmsg.html(msg);
    notification.show();
    $('#notif-close').on("click", function(){

    });
    console.log('Notif : ' + notification.html());
}

function hideNotif(){
    notification = $('#notification');
    notification.css('display', 'none');
}

$(document).ready(function(){
    $("input[type='button']").click(function(){
        if (this.value === 'Register'){
        clear_regerror();
        ajaxCallRegister();
          }
        else if (this.id === 'updprofsub'){
        clear_proferror();
        ajaxCallProfile();
         }

        else if (this.id === 'vehidtl'){
        clear_vehierror();
        ajaxCallVehicle();
         }

        else if (this.value === 'Change Password'){
        clear_chngpass();
        ajaxCallChangePassword();
        }

        else if (this.value === 'Reset Password'){
        clear_resetpass();
        ajaxCallResetPassword();
        }

        else if (this.value === 'Login'){
        clear_loginerror();
        ajaxCallLogin();
         }
    });
});

function clear_loginerror(){
    $("#err_login").empty();
}

function clear_regerror(){
    $("#err_rusername").empty();
    $("#err_email").empty();
    $("#err_password1").empty();
    $("#err_password2").empty();
}


function clear_proferror(){
    $("#err_type").empty();
    $("#err_start_work_time").empty();
    $("#err_end_work_time").empty();
    $("#err_gender").empty();
    $("#err_phone").empty();
}


function ajaxCallLogin(){
    var url = 'login/';
    var data = {};
    data.username = $('#username').val();
    data.password = $('#password').val();
    callAjax('POST', url, data, 'loginError', 'loginSuccess', 'callbacks');
}



function ajaxSyncData(){
    var url = 'sync/';
    var data = {};
    data.user = user;
    data.sky = sky;
    data.did = did;
    data.date = auth.date_time;
    console.log('Calling sync ajax');
    console.log(data.date);

    console.log(did);
    callAjax('POST', url, data, 'syncError', 'syncSuccess', 'callbacks');
}

function callAjax(type, url, rdata, errcallback, succallback, context){
    var data = rdata;
    data.did = did;
    data.sky = sky;
    data.user = user;
    opts.data = data;
    opts.url = domain + url;
    opts.type = type;
    console.log('Ajax Call : ' + data.did + ' , ' + data.sky + ' , ' + data.user + ' , ' + opts.url + ' , ' + JSON.stringify(data));
    $.ajax(opts).done(function (result){
        console.log(result);
        if (!context){
            window[succallback](result);
        } else{
            window[context][succallback](result, data);
        }
    });
}

function ajaxCallRegister(){
    var url = 'register/';
    var data = {};
    data.username = $('#rusername').val();
    data.email = $('#email').val();
    data.password1 = $('#password1').val();
    data.password2 = $('#password2').val();
    callAjax('POST', url, data, 'regError', 'regSuccess', 'callbacks');
}


var callbacks = {
    regSuccess: function (result, data){
        if (result.status === 'f' && result.errorcode === 'REG2'){
                console.log('Register error');
                for (feild in result.err_data){
                var selc = "#err_"+feild;
                $(selc).html(result.err_data[feild]);
            }
        }else if (result.status === 'f' && result.errorcode === 'REG1'){
            alert(result.message);
            setNotif(result.message);
        }
        else{
            setNotif('Activation link has been sent to your email address. Please check your email and activate');
            lsSet('regdone', 'True');
	        $.ui.loadContent("login",false,false,"slide");
            $.ui.clearHistory();
            homelink.attr('href', '#logregr');
        }
    },
    regError: function (){
        console.log("Error registering");
    },
    updProfSuccess: function (result, data){
        if (result.status === 'f' && result.errorcode === 'PRO2'){
            for (feild in result.err_data){
                var selc = "#err_"+feild;
                $(selc).html(result.err_data[feild]);
            }
        }
        else{
            $.when(processTriggers(result.trigger)).done(function(){
                if (data.type === "O"){
                    homelink.attr("href", "#carupd");
                    $.ui.loadContent("vehicle",false,false,"slide");
                } else{
                    //$.when(db.transaction(loadData)).done(function(){
                    setdashparams(true, '#dashboard', 'C', 'register', '');
                    //});

                }
            });
        }
    },
    updProfError: function (){
        console.log("Error Updating Profile");
    },
    updVehiSuccess: function (result, data){
        if (result.status === 'f' && result.errorcode === 'VEH2'){
            for (feild in result.err_data){
                var selc = "#err_"+feild;
                $(selc).html(result.err_data[feild]);
            }
        }
        else{
            console.log('Update Prof trigger : ' + JSON.stringify(result.trigger));
            $.when(processTriggers(result.trigger)).done(function(){
                console.log('make : ' + data.make);
                lsSet('make', data.make);
                lsSet('model', data.model);
                lsSet('fueleff', data.fueleff);
                setdashparams(true, '#dashboard', 'D', 'register', '');
            });
        }
    },
    updVehiError: function (){
        console.log("Error Vehicle Details");
    },

    ResetPassSuccess: function (result, data){
        if (result.status === 'f' && result.errorcode === 'REP2'){
            for (feild in result.err_data){
                var selc = "#err_"+feild;
                $(selc).html(result.err_data[feild]);
            }
        }
        else{
            homelink.attr("href", "#login");
            $.ui.loadContent("login",false,false,"pop", true);
        }
    },
    ResetPassError: function (){
        console.log("Error in Resetting password");        
    },
    syncRequestSuccess: function (result, data){
        if (result.status === 'f' && result.errorcode === 'REP2'){
            console.log('syncRequest failed');
        }
        else{
            var jud = result.data;
            console.log('SyncRequest Data' + JSON.stringify(result.data) + ' len ' + result.data.length);
            if (jud.length > 0){
                lsSet('ju', JSON.stringify(jud[0]));
                $('#requestsbutton').text('1 Request');
            }
            /*
            if (jud.length > 0){
                console.log('jud ' + jud.length);
                for (i=0; i<jud.length;i++){
                    var ju = jud[i];
                    if (ju){
                        var jus = lsget('ju');
                        if (jus){
                            jus[jus.length] = ju;
                            lsSet('ju', JSON.stringify(jus));
                        } else{
                            lsSet('ju', JSON.stringify([ju]));
                        }
                        $('#requestsbutton').text('1 Request');
                    }
                }
            } */
        }
    },
    syncRequestError: function (){
        console.log("Error in syncRequest");
    },
    adrequestSuccess: function (result, data){
        if (result.status === 'f' && result.errorcode === 'REP2'){
            console.log('Approval/Denial failed');
        }
        else{
            setNotif('Your Approval/Denial has been processed');
        }
    },
    adrequestError: function (){
        console.log("Error in adrequest");
    },

    ChangePassSuccess: function (result, data){
        if (result.status === 'f' && result.errorcode === 'CHP2'){
            for (feild in result.err_data){
                var selc = "#err_"+feild;
                $(selc).html(result.err_data[feild]);
            }
        }        
        else{
            setdashparams(true, '#dashboard', 'B', 'login', '');
	    alert('Your password has been changed');
        }
    },
    ChangePassError: function (){
        console.log("Error in Changing Password");
    },

    syncSuccess: function (result, data){
        if (result.status === 'f'){
            console.log("Sync Error");
            ///db.transaction(loadData);
            //$.ui.loadContent("dashboard",true,false,"slide");
        }else{
            var status = processTriggers(result.trigger);
            if (status){
                db.transaction(function (tx){
	        	    tx.executeSql('UPDATE AUTH SET date_time=' + "'" + result.date_time + "'");
                });
            }
	    //$.ui.loadContent("dashboard",true,false,"slide");
	    }
        db.transaction(loadData);
    },
    syncError: function (){
        console.log("Error Syncing");
    },
    loginError: function (){
	console.log('Error in login');
    },
    loginSuccess: function (result, data){
        if (result.status === 'f' && result.errorcode === 'LOG1'){
                console.log('login failed' + result.message);
                $("#err_login").html(result.message);
            }
            else if (result.status === 'f' && result.errorcode === 'LOG2'){
                alert(result.message);
            }
        else if (result.status === 'f' && result.errorcode === 'ACT1'){
                console.log('LOGIN :' + result.message);
                $("#err_login").html(result.message);
            }
        else{
            hideNotif();
            db.transaction(function (tx){
                var sql = "DELETE FROM AUTH";
                   tx.executeSql(sql);
                });
                db.transaction(function (tx){
                    var sql='INSERT INTO AUTH (user,sky, did, date_time) VALUES ("'+data.username+'","'+result.data.key+'","'+result.data.did+'","'+result.data.date_time+ '")';
                    console.log("inserting auth");
                    console.log(sql);
                    tx.executeSql(sql);
                    console.log('After Login Trigger : ' + JSON.stringify(result.trigger));
                    $.when(processTriggers(result.trigger)).done(function(){
                        $.when(db.transaction(function(tx){
                            tx.executeSql('SELECT * FROM AUTH',
                              [],
                              function(tx, results){
                                  if (results.rows.length >= 1){
                                      auth = results.rows.item(0);
                                      did=auth.did;
                                      sky=auth.sky;
                                      user=auth.user;
                                  }
                                  tx.executeSql('SELECT * FROM PROFILE',
                                    [],
                                    function(tx, results){
                                        console.log('Profiel Len ' + results.rows.length);
                                        lsSet('profileLen', results.rows.length);
                                        if (lsget('profileLen') < 1){
                                                                console.log('Profile Not found calling update profile');
                                                                $.ui.loadContent("profupd",false,false,"slide");
                                                                homelink.attr('href', '#profupd');
                                    }
                    					else if (lsget('profileLen') >= 1){
                                            lsSet('profile', results.rows.item(0));
                                            if (result.data.passreset){
					                    	    setNotif('Please change your password');
						                        $.ui.loadContent("changepass",false,false,"slide");
                                            }
					    else{
						setdashparams(true, '#dashboard', 'B', 'login', ''); // Entry point for dash after successful login
                                            }
					}
                                        
                                  },
                                      errorCB
                                    )
                              } , errorCB)
                        })).done(function(){
                        });
                    });
                }, function (){console.log("Login:Error inserting auth")});
        }
    },
    recomSuccess: function(result, data){    	
        if (result.status === 'f'){
            console.log('Recommendations Error See message' + result.message);
        } else{
            console.log("Recom Data : " + JSON.stringify(result.data));
            reloadDash(result.data);
        }
    },
    recomError: function(){
        console.log('Recommendations Error');
    },
    getprofSuccess: function(result, data){
        if (result.status === 'f'){
            console.log('Viewprofile Error See message' + result.message);
        } else{
            console.log("Veiew Profile Data : " + JSON.stringify(result.data));
            viewProfile(result.data);
        }
    }, getprofError: function(){
        console.log('Get Profile Error');
        setNotif('Unable to get profile, Oops some sort of error');
    }, joinUnjoinSuccess: function(result, data){
        if (result.status === 'f'){
            console.log('Join/Unjoin Error See message' + result.message);
        } else{
            console.log("Join Data : " + JSON.stringify(result.data));
            //$.when(processTriggers(result.data)).done(function (){
            setNotif('Your request has been sent to ' + data.poolname + ' for acceptance');
            //});
        }
    }, joinUnjoinError: function(){
        console.log('Get Profile Error');
        setNotif('Unable to contact pool owner, Err!!!!');
    }
};

function setdashparams(clearhist, homehref, point, dashentry, params){
    if (clearhist){
        $.ui.clearHistory();    
    }
    if (homehref){    	
        homelink.attr('href', homehref);
    }
    if (point){
        dashstartpoint = point; // After Login
    }
    if (dashentry){
        //$.ui.loadContent("dashboard",false,false,"slide");
        loadDashboard(dashentry, params, dashstartpoint);
    }
}

function profile_exists(){
    var exists = false;
    db.transaction(function(tx){
	    tx.executeSql('SELECT * FROM PROFILE',
		      [],
		      function(tx, results){
                  console.log('Profiel Len ' + results.rows.length);
                  profileLen = results.rows.length;
		      },
		      errorCB
         )
    });
}

function load_auth(){
    $.when(db.transaction(function(tx){
        tx.executeSql('SELECT * FROM AUTH',
                  [],
                  function(tx, results){
                  if (results.rows.length >= 1){
                      auth = results.rows.item(0);
                      did=auth.did;
                      sky=auth.sky;
                      user=auth.user;
                  }
                    tx.executeSql('SELECT * FROM PROFILE',
                      [],
                      function(tx, results){
                          console.log('Profiel Len ' + results.rows.length);
                          lsSet('profileLen', results.rows.length);
                          lsSet('profile', results.rows.item(0));
                      },
                      errorCB
                    )
                  } , errorCB)
        })).done(function(){
            return;
        })

}

function clear_proferror(){
    $("#err_type").empty();
	$("#err_startlocation").empty();
    $("#err_endlocation").empty();
    $("#err_start_work_time").empty();
    $("#err_end_work_time").empty();
    $("#err_gender").empty();
    $("#err_phone").empty();
}


function ajaxCallProfile(){
    var url = 'profile/';
    var data = {};
    data.type = $('#proftype').val();
    data.startlocation = $('#startlocation').val();
    data.endlocation = $('#endlocation').val();
    data.start_work_time = $('#starttime').val();
    data.end_work_time = $('#endtime').val();
    data.gender = $('#gender').val();
    data.preferred_gender = $('#preferred_gender').val();
    data.phone = $('#phone').val();
    callAjax('POST', url, data, 'updProfError', 'updProfSuccess', 'callbacks');
}

function clear_vehierror(){
    $("#err_car_number").empty();
    $("#err_make").empty();
    $("#err_model").empty();
    $("#err_color").empty();
    $("#err_year").empty();
    $("#err_fueleff").empty();
    $("#err_fuel").empty();
    $("#err_pref_gender").empty();
    $("#err_totalmembers").empty();
    $("#err_occupied").empty();
    $("#err_freeseats").empty();
}

function ajaxCallVehicle(){
    var url = 'vehicle/';
    var data = {};
    data.car_number = $('#car_number').val();
    data.make = $('#make').val();
    data.model = $('#model').val();
    data.color = $('#color').val();
    data.year = $('#year').val();
    data.fueleff = $('#fueleff').val();
    data.fuel = $('#fuel').val();
    data.pref_gender = $('#pref_gender').val();
    data.totalmembers = $('#totalmembers').val();
    data.occupied = $('#occupied').val();
    data.freeseats = $('#freeseats').val();
    opts.error = function(){alert('System Error');};
    callAjax('POST', url, data, 'updVehiError', 'updVehiSuccess', 'callbacks');
}

function clear_chngpass(){
    $("#err_old_password").empty();
    $("#err_new_password1").empty();
    $("#err_new_password2").empty();
}
function ajaxCallChangePassword(){
    var url = 'chngpass/';
    var data = {};
    data.old_password = $('#old_password').val();
    data.new_password1 = $('#new_password1').val();
    data.new_password2 = $('#new_password2').val();
    opts.error = function(){alert('System Error');};
    callAjax('POST', url, data, 'ChangePassError', 'ChangePassSuccess', 'callbacks');
}

$('.notif-close').click(function(){
    ('#notification').hide();
});

function clear_resetpass(){
    $("#err_fusername").empty();
}
function ajaxCallResetPassword(){
    var url = 'resetpass/';
    var data = {};
    data.fusername = $('#fusername').val();
    opts.error = function(){alert('System Error');};
    callAjax('POST', url, data, 'ResetPassError', 'ResetPassSuccess', 'callbacks');
}

function loadDashboard(dashentry, params, dashstartpoint){
    console.log("Home link " + homelink.attr('href'));
    homelink.attr('href', '#dashboard');

    var navbarvehicle = $('#navbar-vehicle');
    var navbarprof = $('#navbar-profupd');
    //var linkpm = $('#link-mypoolmembers');
    var dashfoot = $('#dashboard-footer');    
    var owner = false;
    var membersexist = false;
    $.when(db.transaction(loadData)).done(function(){
        var profileLen = lsget('profileLen');
        var profile = lsget('profile');

        console.log("Profile Len" + profileLen);

        if (profileLen < 1){
            setNotif('Please Update to your profile <a href="#profupd">Update to your profile </a>');
            navbarvehicle.parent().css('display', 'none');
            //linkpm.css('display', 'none');
        }
        else{
            console.log('Prof Type : ' + profile.type);
            if (profile.type === 'O'){
                //linkpm.css('display', 'block');
                owner = true;
            } else{
                owner = false;
                //linkpm.css('display', 'none');
                navbarvehicle.parent().css('display', 'none');
            }
            console.log(owner);
            var msg = (owner) ? 'There are no memebers on the pool. Why dont you get started by adding people to your pool below' :  'Your are not part of a pool, why dont you get started, by browsing recommendations';
            if (poolmembersLen < 1){
                setNotif(msg);
                loadRecommendations(user, profile.type);
                syncRequests();
            } else{
                showMemberPage(owner);
            }
        }

        // On load of App, After Login, After Completion of register
        /* if (point === 'A'){
        } else if (point === 'B'){
            navbarvehicle.parent().css('display', 'none');
            // TODO: We need to redraw the width of each link-footer class based on how many are left
        } else if (point === 'C'){
            navbarvehicle.parent().css('display', 'none');
            linkpm.css('display', 'none');
            // TODO: We need to redraw the width of each link-footer class based on how many are left
        } */
        console.log($('#dashboard').html().slice(0, $('#dashboard').html().length));
        $.ui.loadContent("dashboard",false,false,"slide");
	$.ui.clearHistory();

    });
}

function loadLogin(){
    $('#username').attr("value","");
    $('#password').attr("value","");
    $('#err_login').empty()
}

function loadForgot(){
    hideNotif();
    $('#fusername').attr("value","");
}

function loadRegister(){
    hideNotif();
    $('#rusername').attr("value", "");
    $('#email').attr("value", "");
    $('#password1').attr("value", "");
    $('#password2').attr("value", "");
    clear_regerror();
}

function clearChangePassFrom(){
    hideNotif();
    $('#old_password').attr("value", "");
    $('#new_password1').attr("value", "");
    $('#new_password2').attr("value", "");
    clear_chngpass();
}

function OnDashUnload(){
    hideNotif();
}

function loadProfile(){
    hideNotif();
    var profileLen = lsget('profileLen');
    var profile = lsget('profile');
    console.log('Loadprofile ' + profileLen);
    console.log(JSON.stringify(profile));
    if (profileLen > 0){
        $('#proftype').val(profile.type);
        $('#startlocation').val(profile.startlocation);
        $('#endlocation').val(profile.endlocation);
        $('#starttime').val(profile.start_work_time);
        $('#endtime').val(profile.end_work_time);
        $('#gender').val(profile.gender);
        $('#preferred_gender').val(profile.preferred_gender);
        $('#phone').val(profile.phone);
        $('#skipvehidtl').on('click', function(){loadDashboard('', '', '')});
    }
}

function loadVehicle(){
    hideNotif();

    var poolLen = lsget('poolLen');
    var pool = lsget('pool');
    console.log('Loadpool ' + poolLen);
    console.log(JSON.stringify(pool));
    if (poolLen > 0){
        $('#make').val(lsget('make'));
        $('#model').val(lsget('model'));
        $('#year').val(pool.year);
        $('#fueleff').val(lsget('fueleff'));
        $('#fuel').val(pool.fuel);
        $('#pref_gender').val(pool.pref_gender);
        $('#totalmembers').val(pool.totalmembers);
        $('#occupied').val(pool.occupied);
        $('#freeseats').val(pool.freeseats);
    }
}

function skipVehicle(){
    loadDashboard('', '', '');
}

function loadRecommendations(username, owner){
    var data = {};
    var url = 'recom/';
    data.username = username;
    data.owner = owner;
    callAjax('POST', url, data, 'recomError', 'recomSuccess', 'callbacks');
}
function syncRequests(){
    var data = {};
    var url = 'syncRequests/';
    callAjax('POST', url, data, 'syncRequestError', 'syncRequestSuccess', 'callbacks');
}

function reloadDash(data, calltype, extradata){
    console.log('Into reloadDash');
    var myprof = lsget('profile');

    if (calltype === 'viewprofile'){
        var profresults = $('#viewprofile');
        var msg = 'No profile details found';
    }else{
        var profresults = $('#search-results');
        var msg = 'No search results found';
    }

    if (!data){
        profresults.html('<span class="error">' + msg +'</span>');
        return;
    }

    var profilehtml = '';

    profilehtml +=                    '<div class="prof-wrap" id="##username##">';
    profilehtml +=                        '<div class="prof-avatar-wrap">';
    profilehtml +=                            '<img src="" class="prof-avatar" />';
    profilehtml +=                            '<div class="ratings-wrap">';
    profilehtml +=                                '<span class="icon star"></span>';
    profilehtml +=                                '<span class="icon star"></span>';
    profilehtml +=                                '<span class="icon star"></span>';
    profilehtml +=                                '<span class="icon star"></span>';
    profilehtml +=                                '<span class="icon star"></span>';
    profilehtml +=                            '</div>';
    profilehtml +=                            '<a href="#reviews" class="clickable review">##numreviews## Reviews</a>';
    profilehtml +=                        '</div>';
    profilehtml +=                        '<h3 class="prof-name"><a href="" class="clickable viewprof">##username##</a></h3>';
    profilehtml +=                        '<div class="prof-info-wrap">';
    profilehtml +=                            '<span class="info-name">Start Location</span>';
    profilehtml +=                            '<span class="info-value">##startloc##</span>';
    profilehtml +=                            '<span class="info-name">End Location</span>';
    profilehtml +=                            '<span class="info-value">##endloc##</span>';
    profilehtml +=                            '<span class="info-name">Start Time</span>';
    profilehtml +=                            '<span class="info-value">##starttime##</span>';
    profilehtml +=                            '<span class="info-name">End Time</span>';
    profilehtml +=                            '<span class="info-value">##endtime##</span>';
    profilehtml +=                        '</div>';
    profilehtml +=                        '<div class="prof-right">';
    profilehtml +=                            '<div class="prof-type">';
    profilehtml +=                                '<img src="css/images/##proftypeimg##" class="prof-type-img" />';
    profilehtml +=                            '</div>##ownerinfo##';
    profilehtml +=                            '<input type="button" class="clickable viewprof" value="View Profile">';
    profilehtml +=                        '</div>';
    profilehtml +=                    '</div>';


    var viewprofhtml = '';

    viewprofhtml +=                    '<div class="prof-wrap" id="##username##">';
    viewprofhtml +=                        '<div class="prof-avatar-wrap">';
    viewprofhtml +=                            '<img src="" class="prof-avatar" />';
    viewprofhtml +=                            '<div class="ratings-wrap">';
    viewprofhtml +=                                '<span class="icon star"></span>';
    viewprofhtml +=                                '<span class="icon star"></span>';
    viewprofhtml +=                                '<span class="icon star"></span>';
    viewprofhtml +=                                '<span class="icon star"></span>';
    viewprofhtml +=                                '<span class="icon star"></span>';
    viewprofhtml +=                            '</div>';
    viewprofhtml +=                            '<a href="#reviews" class="clickable review">##numreviews## Reviews</a>';
    viewprofhtml +=                        '</div>';
    viewprofhtml +=                        '<h3 class="prof-name"><a href="" class="notclickable viewprof">##username##</a></h3>';
    viewprofhtml +=                        '<div class="prof-info-wrap">';
    viewprofhtml +=                            '<span class="info-name">Start Location</span>';
    viewprofhtml +=                            '<span class="info-value">##startloc##</span>';
    viewprofhtml +=                            '<span class="info-name">End Location</span>';
    viewprofhtml +=                            '<span class="info-value">##endloc##</span>';
    viewprofhtml +=                            '<span class="info-name">Start Time</span>';
    viewprofhtml +=                            '<span class="info-value">##starttime##</span>';
    viewprofhtml +=                            '<span class="info-name">End Time</span>';
    viewprofhtml +=                            '<span class="info-value">##endtime##</span>';
    viewprofhtml +=                        '</div>';
    viewprofhtml +=                        '<div class="prof-right">';
    viewprofhtml +=                            '<div class="prof-type">';
    viewprofhtml +=                                '<img src="css/images/##proftypeimg##" class="prof-type-img" />';
    viewprofhtml +=                            '</div>##ownerinfo##';
    viewprofhtml +=                        '</div>';
    viewprofhtml +=                         '##preferences##'
    viewprofhtml +=                        '##vehicleinfo##';
    viewprofhtml +=                        '##poolinfo##';
    viewprofhtml +=                        '<input class="enabled join" poolid="##poolid##" joinname="##joinname##" poolname="##poolname##" type="button" value="##joinbutton##" />';
    viewprofhtml +=                    '</div>';

    var owenerinfo = '';
    owenerinfo +=                            '<div class="owner-info">';
    owenerinfo +=                                '<span class="info-name">##car##</span>';
    owenerinfo +=                                '<span class="info-name">Seats Left</span><span class="seats-left-val value">##seatsleft##</span>';
    owenerinfo +=                                '<span class="info-name">Total seats</span><span class="tot-seats-val value">##totseats##</span>';
    owenerinfo +=                            '</div>';

    var vehicleinfo = '';

    vehicleinfo += '<div class="vehicledetails">' +
        '<h3>Vehicle Details</h3>' +
        '<span class="info-name">Year of Car</span><span class="info-value">##yearofcar##</span>' +
        '<span class="info-name">Make of Car</span><span class="info-value">##makeofcar##</span>' +
        '<span class="info-name">Model of Car</span><span class="info-value">##modelofcar##</span>' +
        '<span class="info-name">Fuel type of Car</span><span class="info-value">##fueltypeofcar##</span>' +
        '<span class="info-name">Mileage of Car</span><span class="info-value">##fueleffofcar##</span>' +
        '</div>';

    var poolinfo = '';

    poolinfo += '<div class="pooldetails">' +
        '<h3>Owner Pool Details</h3>' +
        '<span class="info-name">Start Location</span><span class="info-value">##startlocation##</span>' +
        '<span class="info-name">End Location</span><span class="info-value">##endlocation##</span>' +
        '<span class="info-name">Total Travel Distance </span><span class="info-value">##distance##</span>' +
        '<span class="info-name">Start Time</span><span class="info-value">##starttime##</span>' +
        '<span class="info-name">End Time</span><span class="info-value">##endtime##</span>' +
        '<span class="info-name">Preferred Gender</span><span class="info-value">##preferredgender##</span>' +
        '<span class="info-name">Total Seats</span><span class="info-value">##totalseats##</span>' +
        '<span class="info-name">Free Seats</span><span class="info-value">##freeseats##</span>' +
        '</div>';

    profresults.html('');

    var tothtml = '';
    for(i=0;i<data.length;i++){
        var tdata =  data[i];
        if (tdata.username == user){
            continue;
        }
        console.log('tdata : ' + JSON.stringify(tdata));
        var tempprofhtml = (calltype === 'viewprofile') ? viewprofhtml : profilehtml;
        var tempownerinfo = owenerinfo;

        tempprofhtml = tempprofhtml.replace(/##username##/gi, (calltype === 'viewprofile') ? tdata.user : tdata.username);
        tempprofhtml = tempprofhtml.replace(/##numreviews##/gi, (tdata.reviews) ? tdata.review :0 );
        tempprofhtml = tempprofhtml.replace(/##startloc##/gi, (tdata.startlocation) ? tdata.startlocation : 'Unspecified');
        tempprofhtml = tempprofhtml.replace(/##endloc##/gi, (tdata.endlocation) ? tdata.endlocation : 'Unspecified');
        tempprofhtml = tempprofhtml.replace(/##starttime##/gi, (tdata.starttime) ? tdata.starttime : 'Unspecified');
        tempprofhtml = tempprofhtml.replace(/##endtime##/gi, (tdata.endtime) ? tdata.endtime : 'Unspecified');
        tempprofhtml = tempprofhtml.replace(/##joinname##/gi, (calltype === 'viewprofile') ? tdata.user : tdata.username);
        tempprofhtml = tempprofhtml.replace(/##preferences##/gi, ''); // TODO: Expand it with correct prefs
        tvehiclehtml = '';
        tpoolhtml = '';
        owner = (calltype === 'viewprofile') ? tdata.type : tdata.owner;
        if (owner === 'O'){
            tempprofhtml = tempprofhtml.replace(/##proftypeimg##/gi, 'carowner.png');
            tempprofhtml = tempprofhtml.replace(/##joinbutton##/gi, 'Join this Pool');
            tempownerinfo = tempownerinfo.replace(/##car##/gi, (tdata.car) ? tdata.car : 'Unspecified');
            tempownerinfo = tempownerinfo.replace(/##seatsleft##/gi, (tdata.freeseats) ? tdata.freeseats : 'Unspecified');
            tempownerinfo = tempownerinfo.replace(/##totseats##/gi, (tdata.totalseats) ? tdata.totalseats : 'Unspecified');
            if (calltype === 'viewprofile'){
                if (extradata.pooldata){
                    var pooldata = extradata.pooldata;
                    var tpoolhtml = poolinfo;
                    tempprofhtml = tempprofhtml.replace(/##poolid##/gi, pooldata.id);
                    tpoolhtml= tpoolhtml.replace(/##startlocation##/gi, (pooldata.startlocation) ? pooldata.startlocation : 'Unspecified');
                    tpoolhtml= tpoolhtml.replace(/##endlocation##/gi, (pooldata.endlocation) ? pooldata.endlocation : 'Unspecified');
                    tpoolhtml= tpoolhtml.replace(/##starttime##/gi, (pooldata.journey_start_time) ? pooldata.journey_start_time : 'Unspecified');
                    tpoolhtml= tpoolhtml.replace(/##endtime##/gi, (pooldata.journey_end_time) ? pooldata.journey_end_time : 'Unspecified');
                    tpoolhtml= tpoolhtml.replace(/##distance##/gi, (pooldata.distance) ? pooldata.distance : 'Unspecified');
                    tpoolhtml= tpoolhtml.replace(/##preferredgender##/gi, (pooldata.preferred_gender) ? pooldata.preferred_gender : 'Unspecified');
                    tpoolhtml= tpoolhtml.replace(/##totalseats##/gi, (pooldata.totalmembers) ? pooldata.totalmembers : 'Unspecified');
                    tpoolhtml= tpoolhtml.replace(/##freeseats##/gi, (pooldata.freeseats) ? pooldata.freeseats : 'Unspecified');
                } else{
                    tempprofhtml = tempprofhtml.replace(/##poolid##/gi, '');
                }
                if (extradata.vehicledata){
                    var vehicledata = extradata.vehicledata;
                    var tvehiclehtml = vehicleinfo;
                    tvehiclehtml= tvehiclehtml.replace(/##makeofcar##/gi, (vehicledata.make) ? vehicledata.make : 'Unspecified');
                    tvehiclehtml= tvehiclehtml.replace(/##modelofcar##/gi, (vehicledata.model) ? vehicledata.model : 'Unspecified');
                    tvehiclehtml= tvehiclehtml.replace(/##yearofcar##/gi, (vehicledata.year) ? vehicledata.year : 'Unspecified');
                    tvehiclehtml= tvehiclehtml.replace(/##fueleffofcar##/gi, (vehicledata.fueleff) ? vehicledata.fueleff : 'Unspecified');
                    tvehiclehtml= tvehiclehtml.replace(/##fueltypeofcar##/gi, (vehicledata.fuel) ? vehicledata.fuel : 'Unspecified');
                }
            }
        } else{
            if (calltype == 'viewprofile'){
                var mypool =lsget('pool');
                console.log("My Pool :" + JSON.stringify(mypool));
                tempprofhtml = tempprofhtml.replace(/##poolid##/gi, mypool.id);
                tempprofhtml = tempprofhtml.replace(/##poolname##/gi, user);
            }
            tempprofhtml = tempprofhtml.replace(/##poolid##/gi, '');
            tempprofhtml = tempprofhtml.replace(/##joinbutton##/gi, 'Invite to Join your Pool');
            tempprofhtml = tempprofhtml.replace(/##proftypeimg##/gi, 'carmember.png');
            tempownerinfo = '';
        }
        tempprofhtml = tempprofhtml.replace(/##poolname##/gi, tdata.username);
        tempprofhtml = tempprofhtml.replace(/##poolinfo##/gi, tpoolhtml);
        tempprofhtml = tempprofhtml.replace(/##vehicleinfo##/gi, tvehiclehtml);
        tempprofhtml = tempprofhtml.replace(/##ownerinfo##/gi, tempownerinfo);
        profresults.append(tempprofhtml);
        tothtml += tempprofhtml;
        tothtml += tempprofhtml;
    }
    console.log('HTML Data ' + profresults.html());
    //$('#dashboard').append('<span>....</span>--------------------------------------------------');
    //alert($('#dashboard').html().slice($('#dashboard').html().length-2000), $('#dashboard').html().length);
    // Bind the View Profile buttons
    if (calltype === 'viewprofile'){
        $.ui.loadContent("viewprofile",false,false,"slide");
        console.log("Into Loading Profile");
        $('.enabled.join').on('click', function(e){
            if (myprof.type == 'O'){
                var poolname = $(this).attr('joinname');
                var joinname = $(this).attr('poolname');
            }else{
                var joinname = $(this).attr('joinname');
                var poolname = $(this).attr('poolname');
            }
            var poolid = $(this).attr('poolid');
            JoinPool(joinname, poolname, poolid, 'J', myprof.type);
        });
    } else{
        $('.clickable.viewprof').on('click', function(e){
            var id = '';
            if ($(this).is("h3")){
                id = $(this).parent().attr('id');
            }else{
                id = $(this).parent().parent().attr('id');
            }
            getProfile(id);
        });
    }
}

function JoinPool(joinname, poolname, poolid, action, owner){

    var url = 'joinunjoin/';
    var data = {};
    data.user = user;
    data.sky = sky;
    data.did = did;
    data.joinname = joinname;
    data.poolname = poolname;
    data.action = action;
    data.poolid = poolid;
    if (!poolid){
        if (owner =='O'){
            setNotif('Update your Vehicle details, to invite other users to Join Profile');
        } else{
            setNotif('Owner has not created/completed  Pool setup   ');
        }

    } else{
        console.log('Calling Join Unjoin');

        callAjax('POST', url, data, 'joinUnjoinError', 'joinUnjoinSuccess', 'callbacks');

    }
}

function adrequest(action, poolid, requestedby){

    var url = 'approveRequest/';
    var data = {};
    data.user = user;
    data.sky = sky;
    data.did = did;
    data.poolid = poolid;
    data.action = action;
    data.requestedby = requestedby;
    console.log('Calling Approve Join Unjoin');

    callAjax('POST', url, data, 'adrequestError', 'adrequestSuccess', 'callbacks');
}

function loadRequests(){
    var rw = $('#requests-wrap');
    rw.html('');
    var requesthtml = '<div class="request">' +
        '<span class="left-side">Requested By</span>' +
        '<span class="right-side">##requestedby##</span>' +
        '<span class="left-side">Requested Type</span>' +
        '<span class="right-side">##requesttype##</span>' +
        '<input type="button" requestedby="##requestedby##"  poolid="##poolid##" poolid="##poolid##" class="button approve" value="Approve">' +
        '<input type="button" requestedby="##requestedby##"  poolid="##poolid##" class="button deny" value="Deny">' +
    '</div>';
    jus = JSON.parse(lsget('ju'));
    console.log('jus ' + jus + ' ' + jus.length);
    var th = requesthtml;
    th = th.replace(/##requestedby##/gi, jus.requested_by);
    th = th.replace(/##requesttype##/gi, jus.request_type);
    th = th.replace(/##poolid##/gi, jus.id);
    rw.append(th);

    /*
    for (i=0;i<jus.length;i++){
        ju = jus[i];
        var th = requesthtml;
        th = th.replace(/##requestedby##/gi, ju.requested_by);
        th = th.replace(/##requesttype##/gi, ju.request_type);
        th = th.replace(/##poolid##/gi, ju.id);
        rw.append(th);
    } */
    $('.approve').on("click", function(){
        var requestedby = $(this).attr('requestedby');
        var poolid = $(this).attr('poolid');
        console.log('req : ' + requestedby + poolid);
       adrequest('A', poolid, requestedby);
    });
    $('.deny').on("click", function(){
            var requestedby = $(this).attr('requestedby');
            var poolid = $(this).attr('poolid');
           adrequest('D', poolid, requestedby);
    });
}

function getProfile(id){
    console.log('id :' + id);

    var url = 'getprof/';
    var data = {};
    data.user = user;
    data.sky = sky;
    data.did = did;
    data.proftoget = id;
    console.log('Calling Get Profile');

    callAjax('POST', url, data, 'getprofError', 'getprofSuccess', 'callbacks');
}

function viewProfile(data){
    var pooldata = (data.hasOwnProperty('pool')) ? data.pool : '';
    var vehicledata = (data.hasOwnProperty('vehicle')) ? data.vehicle : '';
    var extra = {pooldata: pooldata,
                vehicledata: vehicledata
    };
    profile = [data.profile];
    console.log('calling reloaddash');
    console.log('data : ' + JSON.stringify(data));
    reloadDash(profile, 'viewprofile', extra);
}




/*
                        <div class="prof-wrap" id="userid">
                            <div class="prof-avatar-wrap">
                                <img src="" class="prof-avatar" />
                                <div class="ratings-wrap">
                                    <span class="icon star"></span>
                                    <span class="icon star"></span>
                                    <span class="icon star"></span>
                                    <span class="icon star"></span>
                                    <span class="icon star"></span>
                                </div>
                                <a href="#reviews" class="clickable review">10 Reviews</a>
                            </div>
                            <h3 class="prof-name"><a href="" class="clickable viewprof">Kumar Varadarajulu</a></h3>
                            <div class="prof-info-wrap">
                                <span class="info-name">Start Location</span>
                                <span class="info-value">Hosur, Tamil</span>
                                <span class="info-name">End Location</span>
                                <span class="info-value">Eco Space, Bellandur, KA</span>
                                <span class="info-name">Start Time</span>
                                <span class="info-value">8:30 AM</span>
                                <span class="info-name">End Time</span>
                                <span class="info-value">6:30 PM</span>
                            </div>
                            <div class="prof-right">
                                <div class="prof-type">
                                    <img src="css/images/carowner.png" class="prof-type-img" />
                                </div>
                                <div class="owner-info">
                                    <span class="info-name">Ford Figo</span><span class="color value">Red</span>
                                    <span class="info-name">Seats Left</span><span class="seats-left-val value">2</span>
                                    <span class="info-name">Total seats</span><span class="tot-seats-val value">4</span>
                                </div>
                                <a href="" class="clickable viewprof">View Profile</a>
                            </div>
                        </div>
                        <div class="prof-wrap" id="userid">
                            <div class="prof-avatar-wrap">
                                <img src="" class="prof-avatar" />
                                <div class="ratings-wrap">
                                    <span class="icon star"></span>
                                    <span class="icon star"></span>
                                    <span class="icon star"></span>
                                    <span class="icon star"></span>
                                    <span class="icon star"></span>
                                </div>
                                <a href="#reviews" class="clickable review">10 Reviews</a>
                            </div>
                            <h3 class="prof-name"><a href="" class="clickable viewprof">Kumar Varadarajulu</a></h3>
                            <div class="prof-info-wrap">
                                <span class="info-name">Start Location</span>
                                <span class="info-value">Hosur, Tamil</span>
                                <span class="info-name">End Location</span>
                                <span class="info-value">Eco Space, Bellandur, KA</span>
                                <span class="info-name">Start Time</span>
                                <span class="info-value">8:30 AM</span>
                                <span class="info-name">End Time</span>
                                <span class="info-value">6:30 PM</span>
                            </div>
                            <div class="prof-right">
                                <div class="prof-type">
                                    <img src="css/images/carmember.png" class="prof-type-img" />
                                </div>
                                <div class="owner-info">                                
                                    <span class="info-name">Total seats</span><span class="tot-seats-val value">4</span>
                                 </div>
                                <a href="" class="clickable viewprof">View Profile</a>
                            </div>
                        </div>
                        </div>
 */
